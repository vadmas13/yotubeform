import React, {useEffect} from 'react';
import './App.scss';
import Youtube from "./components/Youtube/Youtube";
import {Route} from "react-router-dom";
import RequestFormContainer from "./components/RequestForm/RequestFormContainer";
import 'antd/dist/antd.css';
import SavedRequestsContainer from "./components/SavedRequests/SavedRequestsContainer";
import ResultDataContainer from "./components/ResultData/ResultDataContainer";
import {connect} from "react-redux";
import Auth from "./components/Auth/Auth";
import {initializeOurApp} from "./redux/appReducer";
import Preloader from "./components/common/Preloader/Preloader";


function App(props) {
    const {app, isAuth} = props;
    const {initialized, checking} = app;

    useEffect(() => {
        props.initializeOurApp(initialized, checking);
    }, [isAuth]);

    if (initialized) {
        return (
            <div className="App">
                <Youtube/>
                <Route exact path='/' render={() => <ResultDataContainer/>}/>
                <Route path='/edit' render={() => <RequestFormContainer/>}/>
                <Route path='/list' render={() => <SavedRequestsContainer/>}/>
            </div>
        );
    }else if(checking) return <Preloader/>;
    else return <Auth/>
}

let mapStateToProps = (state) => {
    return {
        isAuth: state.auth.isAuth,
        app: state.app
    }
}

export default connect(mapStateToProps, {initializeOurApp})(App);

//key: AIzaSyB4QXTGkuKxfEn7WVBoC5XNdDnpjYMdOUg
//&maxResults = 25
//ExampleRequest: www.googleapis.com/youtube/v3/search?part=snippet&q={Your_Query}&type=video&key=AIzaSyB4QXTGkuKxfEn7WVBoC5XNdDnpjYMdOUg