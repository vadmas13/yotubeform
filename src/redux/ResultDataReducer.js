const GET_ITEMS_FROM_API = 'GET_ITEMS_FROM_API';
const CHANGE_FORMAT_DATA = 'CHANGE_FORMAT_DATA';

const initialState = {
    format: {
        list: false,
        blocks: true
    },
    requests: []
}

const ResultDataReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ITEMS_FROM_API:
            return {...state, requests: [...action.items]}
        case CHANGE_FORMAT_DATA:
            return {
                ...state, format: {
                    list: action.format === 'list',
                    blocks: action.format === 'blocks'
                }
            }
        default:
            return state
    }
}

export const getItemsFromResponseApi = (items) => (dispatch) => {
    dispatch({type: GET_ITEMS_FROM_API, items})
}

export const changeFormat = (format) => dispatch => {
    dispatch({type: CHANGE_FORMAT_DATA, format})
}

export default ResultDataReducer