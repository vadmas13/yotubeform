
const CHANGE_TEXT_INPUT = 'CHANGE_TEXT_INPUT';
const REQUEST_LOADING = 'REQUEST_LOADING';
const REQUEST_LOADED = 'REQUEST_LOADED';

const initialState = {
    textInput: '',
    loading: false
}

const SearchReducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_TEXT_INPUT:
            return {...state, textInput: action.value}
        case REQUEST_LOADING:
            return {...state, loading: true}
        case REQUEST_LOADED:
            return {...state, loading: false}
        default: return state
    }
}

export const changeTextInput = (value) => (dispatch) => {
    dispatch({type: CHANGE_TEXT_INPUT, value})
}

export const requestLoading = () => (dispatch) => {
    dispatch({type: REQUEST_LOADING})
}

export const requestLoaded = () => (dispatch) => {
    dispatch({type: REQUEST_LOADED})
}

export default SearchReducer