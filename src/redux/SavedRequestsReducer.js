const GET_VALUE_REQUEST = 'GET_VALUE_REQUEST';
const CREATE_REQUEST = 'CREATE_REQUEST';
const SAVING_PROCESS = 'SAVING_PROCESS';
const SAVED_CHANGES = 'SAVED_CHANGES';
const UPDATE_REQUEST = 'UPDATE_REQUEST';
const GET_REQUEST = 'GET_REQUEST';
const DELETE_REQUEST = 'DELETE_REQUEST';
const UPDATING_REQUESTS_LIST = 'UPDATING_REQUESTS_LIST';
const SET_REQUESTS_LIST = 'SET_REQUESTS_LIST';

const initialState = {
    requestEdit: {
        id: '',
        value: '',
        sortType: '',
        title: '',
        maxResults: 12
    },
    process: {
        updateList: false,
        saving: false
    },
    editMode: {
        value: '',
        sortType: [
            {
                type: 'date',
                title: 'По дате'
            },
            {
                type: 'rating',
                title: 'По рейтингу'
            },
            {
                type: 'title',
                title: 'По заголовку'
            },
            {
                type: 'relevance',
                title: 'Акутальные'
            },
            {
                type: 'viewCount',
                title: 'Количество просмотров'
            }
        ],
        title: '',
        maxResults: 12
    },
    saved: [
        /*{
            id: '1',
            value: 'Приколы видео',
            sortType: 'rating',
            title: 'Приколы',
            maxResults: 30
        },
        {
            id: '2',
            value: 'Строительство Дома',
            sortType: 'viewCount',
            title: 'Дом',
            maxResults: 45
        },
        {
            id: '3',
            value: 'Строительство Дома',
            sortType: 'viewCount',
            title: 'Дом',
            maxResults: 45
        }*/
    ]
}

const SavedRequestsReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_VALUE_REQUEST:
            return {...state, editMode: {...state.editMode, value: action.value}}
        case CREATE_REQUEST:
            debugger;
            return {...state, saved: [...state.saved, {...action.data}], process: {...state.process, saving: false}}
        case SAVING_PROCESS:
            return {...state, process: {...state.process, saving: true}}
        case SAVED_CHANGES:
            return {...state, process: {...state.process, saving: false}}
        case UPDATE_REQUEST:
            return {
                ...state, saved: [...state.saved.map(r => {
                    if (r.id === action.data.id) return {...action.data}
                    else return {...r}
                })], process: {...state.process, saving: false}
            }
        case GET_REQUEST:
            return {...state, requestEdit: {...state.saved.filter(r => r.id === action.id)[0]}}
        case DELETE_REQUEST:
            return {...state, saved: [...state.saved.filter(r => r.id !== action.id)],
                process: {...state.process, updateList: true}}
        case UPDATING_REQUESTS_LIST:
            return {...state, process: {...state.process, updateList: false}}
        case SET_REQUESTS_LIST:
            return {...state, saved: [...action.requests], process: {...state.process, updateList: true}}
        default:
            return state
    }
}

export const getValueRequest = (value) => (dispatch) => {
    dispatch({type: GET_VALUE_REQUEST, value})
}

export const getRequest = (id) => (dispatch) => {
    dispatch({type: GET_REQUEST, id})
}


export default SavedRequestsReducer