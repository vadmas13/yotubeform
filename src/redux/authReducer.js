import {checkingInitializeApp} from "./appReducer";

const AUTH_LOGIN = 'AUTH_LOGIN';
const AUTH_ERROR = 'AUTH_ERROR';
const CHECKING_AUTH = 'CHECKING_AUTH';
const CHECKED_AUTH = 'CHECKED_AUTH';
const SIGN_OUT = 'SIGN_OUT';

const InitState = {
    isAuth: false,
    checkingAuth: false,
    userData: {
        id: '', name: ''
    },
    authError: ''
}

const authReducer = (state = InitState, action) => {
    switch (action.type) {
        case AUTH_LOGIN:
            return {...state, isAuth: true, userData: { id: action.userData.id, name: action.userData.username}, authError: '', checkingAuth: false}
        case AUTH_ERROR:
            return {...state, authError: action.err, checkingAuth: false}
        case CHECKING_AUTH:
            return {...state, checkingAuth: true}
        case CHECKED_AUTH:
            return {...state, checkingAuth: false}
        case SIGN_OUT:
            return {...state, isAuth: false, authId: ''}
        default: return state
    }
}

export const setUserData = (userData, remember) => (dispatch) => {
    const { id } = userData;
    if(remember) localStorage.setItem('user', id);
    else sessionStorage.setItem('user', id);
    dispatch({type: AUTH_LOGIN, userData});
}

export const checkedAuth = () => (dispatch) =>{
    dispatch({type: CHECKED_AUTH})
}

export const signOutUser = () => (dispatch) => {
    dispatch(checkingInitializeApp());
    localStorage.setItem('user', '');
    sessionStorage.setItem('user', '');
    dispatch({type: SIGN_OUT})
}

export default authReducer;