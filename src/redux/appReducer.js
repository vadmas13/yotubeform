import {checkedAuth, signOutUser} from "./authReducer";
import {getUserData} from "./actions/actionAuth";

const INITIALIZED_APP = 'INITIALIZED_APP';
const BREAK_CONNECTION = 'BREAK_CONNECTION';
const CHECKING_INITIALIZE_APP = 'CHECKING_INITIALIZE_APP';
const CHECKED_INITIALIZE_APP = 'CHECKED_INITIALIZE_APP';

const InitState = {
    initialized: false,
    checking: true
}

const appReducer = (state = InitState, action) => {
    switch (action.type) {
        case INITIALIZED_APP:
            return {...state, initialized: true, checking: false}
        case BREAK_CONNECTION:
            return {...state, initialized: false, checking: false}
        case CHECKING_INITIALIZE_APP:
            return {...state, checking: true}
        case CHECKED_INITIALIZE_APP:
            return {...state, checking: false}
        default:
            return state
    }
}

export const initializeOurApp = (initialized, checking) => async (dispatch) => {

    if (!checking) dispatch(checkingInitializeApp())

    let userId = localStorage.getItem('user') ? localStorage.getItem('user') : sessionStorage.getItem('user');
    if (userId) {
        let uName = await dispatch(getUserData(userId));
        if (uName) {
            dispatch({type: INITIALIZED_APP})
            dispatch(checkedAuth());
        } else {
            dispatch(signOutUser());
        }
    } else if (initialized) {
        dispatch({type: BREAK_CONNECTION})
    } else dispatch(checkedInitializeApp())
}

export const checkingInitializeApp = () => dispatch => {
    dispatch({type: CHECKING_INITIALIZE_APP})
}
export const checkedInitializeApp = () => dispatch => {
    dispatch({type: CHECKED_INITIALIZE_APP})
}


export default appReducer;