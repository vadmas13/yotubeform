import firebase from './../../api/firebaseConfig'
import {setUserData} from "../authReducer";

const AUTH_LOGIN = 'AUTH_LOGIN';
const AUTH_ERROR = 'AUTH_ERROR';
const CHECKING_AUTH = 'CHECKING_AUTH';

const db = firebase.firestore();

export const signInUser = (data) => (dispatch) => {
    dispatch({type: CHECKING_AUTH});

    const {username, password, remember} = data;

    db.collection('users').where('username', '==', username)
        .where('password', '==', password).get()
        .then((users) => {
            if (users.docs.length !== 0) {
                let userData = users.docs[0].data();
                dispatch(setUserData(userData, remember))
            } else {
                let err = 'Не такого пользователя';
                dispatch({type: AUTH_ERROR, err});
                console.log(err);
            }
        }).catch(err => {
        console.log(err);
        debugger;
    })
}

export const getUserData = (id) => (dispatch) => {
    return db.collection('users').where('id', '==', id).get()
        .then((users) => {
                if (users.docs.length !== 0) {
                    let userData = users.docs[0].data();
                    dispatch(setUserData(userData))
                    return userData.username
                } else {
                    let err = 'Не такого пользователя';
                    dispatch({type: AUTH_ERROR, err});
                    return false
                }
            }
        )
}