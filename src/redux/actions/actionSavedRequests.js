import firebase from './../../api/firebaseConfig'
import { v4 as uuidv4 } from 'uuid';

const db = firebase.firestore();

const CREATE_REQUEST = 'CREATE_REQUEST';
const SAVING_PROCESS = 'SAVING_PROCESS';
const SAVED_CHANGES = 'SAVED_CHANGES'
const UPDATE_REQUEST = 'UPDATE_REQUEST';
const DELETE_REQUEST = 'DELETE_REQUEST';
const UPDATING_REQUESTS_LIST = 'UPDATING_REQUESTS_LIST';
const SET_REQUESTS_LIST = 'SET_REQUESTS_LIST';

export const createRequest = (values, updateId, userId) => (dispatch) => {
    dispatch({type: SAVING_PROCESS});

    if(!updateId) {
        let id = uuidv4();
        let data = {...values, id, userId}

        db.collection('requests').add({
            ...data
        }).then(() => {
            dispatch({type: SAVED_CHANGES})
        })

    } else{
        let data = {...values, id: updateId};
        db.collection('requests').where('id', '==', updateId).get()
            .then((document) => {
                let dataDocument = document.docs[0].data();
                db.collection('requests').doc(document.docs[0].id).set({
                    ...dataDocument, ...values
                }).then(() =>  dispatch({type: UPDATE_REQUEST, data}))
            })

    }
}

export const deleteRequest = (id) => (dispatch) => {
    dispatch({type: UPDATING_REQUESTS_LIST});
    db.collection('requests').where('id', '==', id).get()
        .then((document) => {
            db.collection('requests').doc(document.docs[0].id).delete()
                .then(() =>  dispatch({type: DELETE_REQUEST, id}))
        })
}

export const getRequestList = (userId) => dispatch => {
    let requests = [];
    dispatch({type: UPDATING_REQUESTS_LIST});
    db.collection('requests').where('userId', '==', userId).get()
        .then(async (document) => {
            await document.docs.map(doc => requests.unshift(doc.data()))
        }).then(() => dispatch({type: SET_REQUESTS_LIST, requests}))

}