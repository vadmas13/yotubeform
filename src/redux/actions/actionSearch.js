import {videosApi} from "../../api/api";
import {getItemsFromResponseApi} from "../ResultDataReducer";
import * as moment from 'moment';
import 'moment/locale/ru';
import {requestLoaded, requestLoading} from "../SearchReducer";


const translateViewCounts = (number) => {
    let divideNumberByPieces = (x, delimiter) => {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, delimiter || "_");
    }
    let viewCount = divideNumberByPieces(number).split('_');
    let isABig = parseInt(viewCount[0]) / 100 > 1;
    switch (viewCount.length) {
        case 2:
            return !isABig ? viewCount[0] + ',' + viewCount[1][0] + ' тыс. просмотров' : viewCount[0] + ' тыс. просмотров';
        case 3:
            return !isABig ? viewCount[0] + ',' + viewCount[1][0] + ' млн. просмотров' : viewCount[0] + ' млн. просмотров';
        case 4:
            return !isABig ? viewCount[0] + ',' + viewCount[1][0] + ' млрд. просмотров' : viewCount[0] + ' млрд. просмотров';
        default:
            return viewCount[0] + 'просмотра';
    }
}

export const searchDataWithKeywords = (keywords, params) => (dispatch) => {
    dispatch(requestLoading());
    videosApi.getSnippetWithKeywords(keywords, params).then(async snippetResponse => {
        let resultData = [];
        for(const item of snippetResponse){
            let statisticsResponse = await videosApi.getStatisticsWithId(item.id.videoId);
            let channelResponse = await videosApi.getChannelInfo(item.snippet.channelId);
            let statistics =  {...statisticsResponse.data.items[0].statistics};
            let viewCount =  translateViewCounts(statistics.viewCount); // translate ViewCount to normal view
            let publishedAt =  moment(item.snippet.publishedAt).fromNow(); // translate Date
            resultData.push({...item, statistics: {...statistics, viewCount},
                snippet: {...item.snippet, publishedAt},
                channelImage: channelResponse.data.items[0].snippet.thumbnails.default.url
            });
            if (resultData.length === snippetResponse.length) {
                dispatch(requestLoaded());
                dispatch(getItemsFromResponseApi(resultData))
            }
        }

    })
}
