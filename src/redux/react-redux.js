import {applyMiddleware, combineReducers, createStore} from 'redux';
import thunkMiddleware from "redux-thunk"
import ResultDataReducer from "./ResultDataReducer";
import SearchReducer from "./SearchReducer";
import SavedRequestsReducer from "./SavedRequestsReducer";
import authReducer from "./authReducer";
import appReducer from "./appReducer";

let reducers = combineReducers({
    resultData: ResultDataReducer,
    search: SearchReducer,
    requests: SavedRequestsReducer,
    auth: authReducer,
    app: appReducer
})

let store = createStore(reducers, applyMiddleware(thunkMiddleware));

export default store;
