import * as axios from "axios";

const KEY = 'AIzaSyCANCixBuRVelr5vBO33g7ahRiKfGd35pw'; //'AIzaSyB4QXTGkuKxfEn7WVBoC5XNdDnpjYMdOUg';
const QUANTITY_ITEMS = 12;
const ORDER_DEFAULT = 'relevance';
const SNIPPET_PARAMS = {
    key: KEY,
    part: 'snippet',
    type: 'video'
}

const STATSTICS_PARAMS = {
    key: KEY,
    part: 'statistics',
    type: 'video'
}

const CHANNEL_PARAMS = {
    key: KEY,
    part: 'snippet'
}

const dataAxios = axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3'
});

export const videosApi = {
    getSnippetWithKeywords(keywords, params) {
        return dataAxios.get('search', {
            params: {
                q: keywords,
                ...SNIPPET_PARAMS,
                order: params && params.sortType ? params.sortType: ORDER_DEFAULT,
                maxResults: params && params.maxResults ? params.maxResults : QUANTITY_ITEMS,
            }
        }).then(response => response.data.items)
    },
    async getStatisticsWithId (itemId){
        return  await dataAxios.get('videos', {
            params: {
                id: itemId,
                ...STATSTICS_PARAMS
            }
        })
    },
    getChannelInfo(channelId){
        return dataAxios.get('channels', {
            params : {
                ...CHANNEL_PARAMS,
                id: channelId
            }
        })
    }

}

