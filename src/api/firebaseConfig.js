import * as firebase from "firebase/app";

import "firebase/firestore";

var firebaseConfig = {
    apiKey: "AIzaSyDRLDa91NJgsQUtZ_c4HroCh_x1A8FE_Lo",
    authDomain: "api-6d47e.firebaseapp.com",
    databaseURL: "https://api-6d47e.firebaseio.com",
    projectId: "api-6d47e",
    storageBucket: "api-6d47e.appspot.com",
    messagingSenderId: "903531859342",
    appId: "1:903531859342:web:957417e0fcd69c02ca68df"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export default firebase;