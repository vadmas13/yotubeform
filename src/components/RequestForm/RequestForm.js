import React from 'react'
import styles from './RequestForm.module.scss'
import {Form, Input, Select, Button, Slider} from 'antd';
import {Typography} from 'antd';
import Preloader from "../common/Preloader/Preloader";


const {Title} = Typography;
const {Option} = Select;

const RequestForm = (props) => {
    const {params, editMode, editRequest, updatedState, valueRequest, userId} = props; //http://localhost:3000/edit/?value=css&title=%D0%94%D0%BE%D0%BC&sortType=rate&maxResults=35
    const updateId = params.id ? params.id : false;

    const onFinish = values => {
        props.createRequest({...values, value: params.value ? params.value : values.value}, updateId, userId);
    }


    if ((editRequest && editRequest.id && params.id && updatedState) || valueRequest) {
        return (
            <div className={styles.SavedRequests}>
                <Title className={styles.SavedRequests__title}>Сохранить запрос</Title>
                <div className={styles.SavedRequests__form}>
                    <Form name="complex-form" onFinish={onFinish}
                          initialValues={{...editRequest}}
                          labelCol={{span: 8}} wrapperCol={{span: 16}}>
                        <Form.Item label="Запрос" className={styles.SavedRequests__field}>
                            <Form.Item
                                name="value"
                                className={styles.SavedRequests__input}
                            >
                                <Input defaultValue={editRequest.value ? editRequest.value : valueRequest}
                                       disabled={!!params.value}/>
                            </Form.Item>
                        </Form.Item>
                        <Form.Item label="Название" className={styles.SavedRequests__field}>
                            <Form.Item
                                name="title"
                                className={styles.SavedRequests__input}
                                rules={[{required: true, message: 'Заголовок не введен'}]}
                            >
                                <Input defaultValue={editRequest.title ? editRequest.title : ''}
                                       placeholder="Обзовите запрос"/>
                            </Form.Item>
                        </Form.Item>
                        <Form.Item label="Сортировать" className={styles.SavedRequests__field}>
                            <Form.Item
                                name={'sortType'}
                                className={styles.SavedRequests__input}
                                rules={[{required: true, message: 'Сортировка не установлена'}]}
                            >
                                <Select placeholder="Тип сортировки"
                                        defaultValue={editRequest.sortType ? editRequest.sortType : ''}>
                                    {editMode && editMode.sortType.map((item, i) => {
                                        return <Option value={item.type} key={i}>{item.title}</Option>
                                    })}
                                </Select>
                            </Form.Item>
                        </Form.Item>
                        <Form.Item label="Количество" className={styles.SavedRequests__field}
                                   name={'maxResults'}
                        >
                            <Slider defaultValue={editRequest.maxResults ? editRequest.maxResults : editMode.maxResults}
                                    min={12}
                                    max={50}/>
                        </Form.Item>
                        <Form.Item label=" " colon={false}>
                            <Button type="primary" htmlType="submit">
                                Внести изменения
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        )
    }else return <Preloader />
}


export default RequestForm