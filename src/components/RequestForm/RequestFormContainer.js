import React, {useEffect, useState}  from 'react'
import {connect} from "react-redux";
import RequestForm from "./RequestForm";
import {Redirect, withRouter} from "react-router-dom";
import {createRequest} from "../../redux/actions/actionSavedRequests";
import {compose} from "redux";
import queryString from 'query-string'
import {getRequest} from "../../redux/SavedRequestsReducer";


const RequestFormContainer = (props) => {
    const { params, requests } = props;
    const {requestEdit} = requests;
    const [editRequest, setEditRequest] = useState(!params.value ? requestEdit : {});
    const [refresh, setRefresh] = useState(false);
    const [updatedState, setUpdatedState] = useState(false);
    const [valueRequest, setValueRequest] = useState(params.value)

    useEffect(() => {
        if(params.id && !refresh && !params.value) {
            props.getRequest(params.id)
            setRefresh(true);
        }
    },[params.id, {...requestEdit}])

    useEffect(() => {
        if(params.id && requestEdit.id === params.id && refresh
            && Object.keys(requestEdit).some(key => requestEdit[key] !== editRequest[key])) {
            setEditRequest(requestEdit);
            setUpdatedState(true);
        }

    },[{...requestEdit}])

    useEffect(() => {
        setValueRequest(params.value)
        setEditRequest({})
    },[params.value])

    // requestEdit.id, requestEdit.title, requestEdit.maxResults, requestEdit.sortType, requestEdit.value

    if(requests.process.saving) return <Redirect to={'/list'} />
    if(params.value || params.id) return <RequestForm editMode={props.editMode}
                                                      userId={props.userId}
                                                      updatedState={updatedState}
                                                      setRefresh={setRefresh}
                                                      valueRequest={valueRequest}
                                                      createRequest={props.createRequest}
                                                      params={params} editRequest={editRequest}/> ;
    else return <Redirect to={'/'} />

}

let mapStateToProps = (state, ownProps) => {
    return{
        userId: state.auth.userData.id,
        params: ownProps.location.search ? queryString.parse(ownProps.location.search) : false,
        requests: state.requests,
        editMode: state.requests.editMode
    }
}



export default compose(
    withRouter,
    connect(mapStateToProps, {createRequest, getRequest}))
(RequestFormContainer)