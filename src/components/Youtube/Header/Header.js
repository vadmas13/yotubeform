import React from 'react'
import styles from './Header.module.scss'
import {Button, Avatar} from 'antd';
import {Link} from "react-router-dom";
import { UserOutlined } from '@ant-design/icons';
import {connect} from "react-redux";
import {signOutUser} from "../../../redux/authReducer";


const Header = (props) => {
    const {id, name} = props.userData;

    let logOut = () => {
        props.signOutUser();
    }

    return (
        <div className={styles.Header}>
            <div className={styles.Header__avatar}>
                <Avatar style={{ backgroundColor: '#87d068' }} icon={<UserOutlined />}  />
                <span className={styles.Header__text}> { name } </span>
                <Button
                    size="small"
                    style={{
                        marginLeft: 16,
                        verticalAlign: 'middle',
                    }}
                    onClick={logOut}
                >
                    Выйти
                </Button>
            </div>
            <Link to={'/list'}>
                <Button block>
                    Сохраненные запросы
                </Button>
            </Link>
        </div>
    )
}
let mapStateToProps = (state) => {
    return {
        userData: state.auth.userData
    }
}

export default connect( mapStateToProps, {signOutUser})(Header)