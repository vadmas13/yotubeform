import React from 'react'
import styles from './Search.module.scss'
import {Button} from 'antd';
import {SearchOutlined} from '@ant-design/icons';
import {Input} from 'antd';
import {Link, Redirect} from "react-router-dom";


const {Search} = Input;


const SearchRequest = (props) => {
    const {search} = props;
    const {textInput, loading} = search;

    let handleSubmit = (e) => {
        if (typeof e !== 'string') e.preventDefault();
        props.searchDataWithKeywords(textInput);
    }

    let handleChange = (e) => {
        let value = e.currentTarget.value;
        props.changeTextInput(value);
    }

    let addRequest = () => {
        props.getValueRequest(textInput);
    }

    if (window.location.pathname && window.location.pathname !== '/' && loading) return <Redirect to={'/'}/>;
    else return (
        <div className={styles.Search}>
            <form>

                <Search value={textInput} className={styles.Search__input} onPressEnter={(e) => handleSubmit(e)}
                        placeholder="Введите запрос" onChange={(e) => handleChange(e)} onSearch={(e) => handleSubmit(e)}
                        enterButton/>
                <Link onClick={addRequest} to={`/edit/?value=${textInput}`}>
                    <Button className={styles.Search__button} type="primary">Сохранить запрос</Button>
                </Link>
            </form>
        </div>
    )
}

export default SearchRequest