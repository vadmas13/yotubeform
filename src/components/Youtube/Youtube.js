import React from 'react'
import styles from './Youtube.module.scss'
import SearchRequest from "./Search/SearchRequest";
import {connect} from "react-redux";
import {searchDataWithKeywords} from "../../redux/actions/actionSearch";
import {changeTextInput} from "../../redux/SearchReducer";
import {getValueRequest} from "../../redux/SavedRequestsReducer";
import Header from "./Header/Header";


const Youtube = (props) => {
    const { params } = props;
    if(params) debugger

    return(
        <div className={styles.Youtube}>
            <Header />
            <SearchRequest search={props.search} changeTextInput={props.changeTextInput}
                           getValueRequest={props.getValueRequest}
                searchDataWithKeywords={props.searchDataWithKeywords}/>
        </div>
    )
}

let mapStateToProps = (state, ownProps) => {
    return{
        search: state.search
    }
}



export default connect(mapStateToProps, {searchDataWithKeywords, changeTextInput, getValueRequest})(Youtube)