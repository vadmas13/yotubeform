import React from 'react'
import {connect} from "react-redux";
import ResultData from "./ResultData";
import Preloader from "../common/Preloader/Preloader";
import {changeFormat} from "../../redux/ResultDataReducer";

const ResultDataContainer = (props) => {
    const {search, resultData} = props;
    const {loading} = search;


    if(resultData.requests.length !== 0 && !loading) return <ResultData resultData={resultData.requests}
                                                                        changeFormat={props.changeFormat}
                                                               format={resultData.format} />
    else if(loading) return <Preloader />
    else return null

}

let mapStateToProps = (state) => {
    return{
        search: state.search,
        resultData: state.resultData
    }
}

export default connect(mapStateToProps, {changeFormat})(ResultDataContainer)
