import React from 'react'
import styleList from './ResultItem.module.scss'
import styleBlocks from './ResultItemBlocks.module.scss'
import {Avatar} from "antd";

const ResultItem = (props) => {
    const {resultItem, format} = props;
    const styles = format.list ? styleList : styleBlocks;

    return (
        <div className={styles.ResultItem} id={resultItem.id.videoId}>
            <div className={styles.ResultItem__image}>
                <img src={resultItem.snippet.thumbnails.medium.url}
                     alt={resultItem.snippet.title}/>
            </div>
            <div className={`${styles.ResultItem__info} ${styles.ItemInfo}`}>
                {format.blocks ?
                    <div className={styles.ItemInfo__channelImage}>
                        <Avatar src={resultItem.channelImage} size={36}/>
                    </div>
                    : null}
                <div className={styles.ItemInfo__about}>
                    <div className={styles.ItemInfo__header}>
                        <a href="#" className={styles.ItemInfo__title}>{resultItem.snippet.title} </a>
                    </div>
                    <div className={styles.ItemInfo__info}>
                        <a href="#"
                           className={`${styles.ItemInfo__text} ${styles.ItemInfo__views} ${styles.ItemInfo__chanel}`}>
                            {resultItem.snippet.channelTitle}
                        </a>
                        <a href="#" className={`${styles.ItemInfo__text} ${styles.ItemInfo__views} `}>
                            {resultItem.statistics.viewCount}
                        </a>
                        <a href="#" className={`${styles.ItemInfo__text} `}>
                            {resultItem.snippet.publishedAt}
                        </a>
                    </div>
                    <div className={styles.ItemInfo__description}>
                        <p className={styles.ItemInfo__text}>{resultItem.snippet.description}</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ResultItem