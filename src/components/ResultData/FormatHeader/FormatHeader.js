import React from 'react'
import styles from './FormatHeader.module.scss'
import {UnorderedListOutlined, AppstoreOutlined} from '@ant-design/icons';


const FormatHeader = (props) => {
    const { format } = props;

    let changeFormatData = (e) => {
        let format = e.currentTarget.id;
        props.changeFormat(format)
    }

    return (
        <div className={styles.FormatHeader}>
            <span onClick={changeFormatData} id={'list'} className={`${styles.FormatHeader__icon} ${format.list ? styles.FormatHeader__activeIcon : ''}`}>
                <UnorderedListOutlined/>
            </span>
            <span onClick={changeFormatData} id={'blocks'} className={`${styles.FormatHeader__icon} ${format.blocks ? styles.FormatHeader__activeIcon : ''}`}>
                <AppstoreOutlined/>
            </span>
        </div>
    )
}

export default FormatHeader