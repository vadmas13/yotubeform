import React from 'react'
import stylesList from './ResultData.module.scss'
import stylesBlocks from './ResultDataBlocks.module.scss'
import ResultItem from "./ResultItem/ResultItem";
import FormatHeader from "./FormatHeader/FormatHeader";

const ResultData = (props) => {
    const {resultData, format} = props;
    let styles = format.list ? stylesList : stylesBlocks
    return (
        <div className={styles.ResultData}>
            <FormatHeader format={props.format} changeFormat={props.changeFormat}/>
            <div className={styles.ResultData__item}>
                {resultData.map((item, i) => {
                    return <ResultItem resultItem={item} key={i} format={format}/>
                })}
            </div>
        </div>
    )
}

export default ResultData