import React from 'react'
import {connect} from 'react-redux'
import styles from './Auth.module.scss'
import {signInUser} from "../../redux/actions/actionAuth";
import {Form, Input, Button, Checkbox, Empty} from 'antd';

const Auth = (props) => {
    const { auth } = props;
    const { authError, checkingAuth } = auth;

    const layout = {
        labelCol: { span: 8 },
        wrapperCol: { span: 16 },
    };
    const tailLayout = {
        wrapperCol: { offset: 8, span: 16 },
    };

    const onFinish = values => {
        props.signInUser(values)
    };


    return(
        <div className={styles.Auth}>
            <Form
                {...layout}
                name="basic"
                initialValues={{ remember: true }}
                onFinish={onFinish}
            >
                <Form.Item
                    label="Логин"
                    name="username"
                    rules={[{ required: true, message: 'Please input your username!' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Пароль"
                    name="password"
                    rules={[{ required: true, message: 'Please input your password!' }]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item {...tailLayout} name="remember" valuePropName="checked">
                    <Checkbox>Запомнить меня</Checkbox>
                </Form.Item>

                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit" loading={!!checkingAuth}>
                        Войти
                    </Button>
                </Form.Item>
            </Form>
            {
                authError && !checkingAuth ? <Empty description={authError}/> : null
            }
        </div>
    )
}

let mapStateToProps = (state) => {
    return{
        auth: state.auth
    }
}


export default connect(mapStateToProps, {signInUser})(Auth)