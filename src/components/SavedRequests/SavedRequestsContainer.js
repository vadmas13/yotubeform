import React, {useEffect, useState} from 'react'
import {connect} from "react-redux";
import SavedRequests from "./SavedRequests";
import {searchDataWithKeywords} from "../../redux/actions/actionSearch";
import {changeTextInput} from "../../redux/SearchReducer";
import {deleteRequest, getRequestList} from "../../redux/actions/actionSavedRequests";
import Preloader from "../common/Preloader/Preloader";




const SavedRequestsContainer = (props) => {
    const { userId } = props;
    const { saving, updateList } = props.requests.process;
    const [requests, setRequests] = useState(props.requests.saved);


    useEffect(() => {
        setRequests(props.requests.saved);
    }, [saving, props.requests.saved.length])

    useEffect(() => {
        props.getRequestList(userId);
    }, [userId])

   if(updateList) return <SavedRequests requests={requests}
                          deleteRequest={props.deleteRequest}
                          sortTypes={props.requests.editMode.sortType}
                          changeTextInput={props.changeTextInput}
                          searchDataWithKeywords={props.searchDataWithKeywords}/> ;
    else return <Preloader />
}

let mapStateToProps = (state, ownProps) => {
    return {
        userId: state.auth.userData.id,
        requests: state.requests
    }
}


export default connect(mapStateToProps, {
    searchDataWithKeywords,
    getRequestList,
    changeTextInput,
    deleteRequest})(SavedRequestsContainer)