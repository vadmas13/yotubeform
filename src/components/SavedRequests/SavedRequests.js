import React from 'react'
import styles from './SavedRequests.module.scss'
import {Typography, PageHeader, Button, Empty, Table} from 'antd';
import {Link} from "react-router-dom";
import {columns} from './TablePattern/TablePattern'


const {Title} = Typography;

const SavedRequests = (props) => {
    const {requests, sortTypes} = props;

    let useRequest = (e) => {
        let id = e.target.id;
        let params = requests.filter(r => r.id === id)[0];
        props.changeTextInput(params.value);
        props.searchDataWithKeywords(params.value, params);
    }

    let deleteRequest = (e) => {
        let id = e.target.id.split('_')[1];
        props.deleteRequest(id);
    }


    return (
        <div className={styles.SavedRequests}>
            <Title>Сохраненные запросы</Title>
            {requests && requests.length !== 0 ?

                requests.map((r, i) => {
                    let data = [{
                        key: i,
                        request: r.value,
                        order: sortTypes.filter(s => s.type === r.sortType)[0].title,
                        maxResults: r.maxResults
                    }]
                    return (
                        <div className={styles.SavedRequests__request}>
                            <div className={styles.SavedRequests__header}>
                                <PageHeader
                                    ghost={false}
                                    title={r.title}
                                    extra={[
                                        <Button onClick={useRequest} id={ r.id } key="1">Применить</Button>,
                                        <Link to={`/edit/?id=${r.id}`}>
                                            <Button  id={`up_${r.id}`} key="3">Изменить</Button>
                                        </Link>,
                                        <Button onClick={deleteRequest} key="2" id={`del_${r.id}`}>Удалить</Button>,
                                    ]}
                                />
                            </div>

                            <Table columns={columns} dataSource={data} pagination={{hideOnSinglePage: true}}/>
                        </div>
                    )
                })

                :  <Empty description={'Нет сохраненных запросов'}/>
            }
        </div>
    )
}


export default SavedRequests