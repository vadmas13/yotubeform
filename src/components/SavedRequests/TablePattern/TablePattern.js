import React from 'react'

export const columns = [
    {
        title: 'Запрос',
        dataIndex: 'request',
        key: 'request',
        render: text => text,
    },
    {
        title: 'Сортировка',
        dataIndex: 'order',
        key: 'order',
    },
    {
        title: 'Количество видео',
        dataIndex: 'maxResults',
        key: 'maxResults',
    }
];
