import React from 'react'
import styles from './Preloader.module.scss'
import { Spin } from 'antd';

const Preloader = () => {

    return(
        <div className={styles.Preloader}>
            <Spin size="large" />
        </div>
    )
}

export default Preloader