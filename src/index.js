import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import store from './redux/react-redux.js';
import {Provider} from "react-redux";
import {BrowserRouter} from "react-router-dom";

window.store = store;

ReactDOM.render(
    <BrowserRouter>
        <Provider store={store}>
    <App />
        </Provider>
    </BrowserRouter>
  ,
  document.getElementById('root')
);



